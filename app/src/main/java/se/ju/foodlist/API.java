package se.ju.foodlist;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.icu.util.Output;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Struct;
import java.util.Scanner;

/**
 * Created by denni on 2017-11-23.
 */

public abstract class API {
    protected static String secret = "jRYJ488gGymshfn0yMIxBuTUj48jp1n9BHyjsnejIMqc1cL58t";
    public static String url = "https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/";

    public enum RequestMethod { GET, POST }

    public static String RequestMethod(RequestMethod requestMethod) {
        switch (requestMethod) {
            case GET: return "GET";
            case POST: return "POST";
            default: return "";
        }
    }

    public static class RequestProperty {
        public String Key;
        public String Value;

        public RequestProperty(String Key, String Value) {
            this.Key = Key;
            this.Value = Value;
        }
    }

    public static HttpURLConnection createConnection(String url, RequestMethod requestMethod, RequestProperty... requestProps) {
        try {
            HttpURLConnection connection = (HttpURLConnection)(new URL(API.url + url)).openConnection();
            connection.setRequestMethod(RequestMethod(requestMethod));
            connection.addRequestProperty("X-Mashape-Key", secret);

            for (RequestProperty prop : requestProps) connection.addRequestProperty(prop.Key, prop.Value);

            return connection;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    // This class can only be used with a single url
    public static class Get extends AsyncTask<String, Integer, JSONObject> {
        @Override
        protected JSONObject doInBackground(String... url) {
            // Todo: Add support for multiple urls which each returns a JSONOjbect?
            if (url.length != 1) {
                System.err.println("There must be exactly one url!");
                return null;
            }

            try {
                HttpURLConnection connection = API.createConnection(url[0], RequestMethod.GET, new API.RequestProperty("Accept", "application/json"));

                int statusCode = connection.getResponseCode();
                Scanner scanner = new Scanner(connection.getInputStream());
                String line = scanner.nextLine();

                System.out.println("\n\n==========================\n\nAPI.Get.doInBackground: What's the line ? " + line + "\n\n==========================\n");

                scanner.close();
                connection.disconnect();
                if (statusCode == 200) return new JSONObject(line);
                else {
                    throw new Exception("Status code not 200!!");
                }
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Exception in API.Get.doInBackground: " + e.getMessage());
                return null;
            }
        }
    }

    public class PostParam {
        public String Url;
        public String Message;
    }

    // Todo: Implement post if needed
    public static class Post extends AsyncTask<PostParam, Void, Void> {
        @Override
        protected Void doInBackground(PostParam... postParams) {
            // Todo: Add support for multiple posts which each returns a unique JSONOjbect?
            if (postParams.length != 1) {
                System.err.println("There must be exactly one url!");
                return null;
            }

            HttpURLConnection connection = API.createConnection(postParams[0].Url, RequestMethod.POST, new API.RequestProperty("Content-Type", "application/json"));

            try {
                OutputStream outputStream = connection.getOutputStream();
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
                outputStreamWriter.write(postParams[0].Message);

                outputStreamWriter.close();
                outputStream.close();

                int statusCode = connection.getResponseCode();
                connection.disconnect();

                if (statusCode == 200) {

                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    // Getting an image (bitmap) async. Calls handler with image on message.obj
    public static void getBitmapFromURL(final String src, final Handler handler) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(src);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    InputStream input = connection.getInputStream();
                    Bitmap bitImage = BitmapFactory.decodeStream(input);

                    Message msg = new Message();
                    msg.obj = bitImage;

                    handler.sendMessage(msg);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
