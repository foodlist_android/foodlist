package se.ju.foodlist;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import java.util.Calendar;

/**
 * Created by Fredrik on 2017-12-08.
 */

public abstract class utils {
    public static int getTodaysDay() {
        Calendar clndr = Calendar.getInstance();
        int currentDay = clndr.get(Calendar.DAY_OF_WEEK);
        // Since the DAY_OF_WEEk returns wonky values, we switch it for more appropriate integers:
        switch (currentDay) {
            case 1: // Sunday
                return 6;
            case 2: // Monday
                return 0;
            case 3: // Tuesday
                return 1;
            case 4: // Wednesday
                return 2;
            case 5: // Thursday
                return 3;
            case 6: // Friday
                return 4;
            case 7: // Saturday
                return 5;
            default:
                return 666; // Sumting wong
        }
    }
    public static int getTodaysWeek() {
        Calendar calendar = Calendar.getInstance();
        Log.i("Calendar stuff", String.valueOf(calendar.get(Calendar.WEEK_OF_YEAR)));
        return calendar.get(Calendar.WEEK_OF_YEAR);
    }
    // This function assumes that there is an incoming message with obj that contains the message
    public static Handler getToastCallback(final Context context) {
        return new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message message) {
                Log.i("Response", "Just got response, the message is: " + (String)message.obj);
                Toast toast = Toast.makeText(context.getApplicationContext(), (String)message.obj, Toast.LENGTH_LONG);
                toast.show();
            }
        };
    }
}
