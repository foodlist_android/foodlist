package se.ju.foodlist;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import org.json.JSONException;
import org.json.JSONObject;

public class MyPreferences extends AppCompatActivity implements View.OnClickListener {
    public static final String PREFS_NAME = "MyPrefsFile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_preferences);
        final String[] spinnerArray = new String[] {
                "All-eater",
                "Ketogenic",
                "Paleo",
                "Primal",
                "Vegetarian",
                "Vegan",
                "Whole 30"
        };

        // set string-values
        ((TextView) (findViewById(R.id.myPreferencesLabel))).setText("My Preferences");
        ((TextView) (findViewById(R.id.dietLabel))).setText("My Diet...");
        ((TextView) (findViewById(R.id.textView5))).setText("My Allergies...");

        ((CheckBox) (findViewById(R.id.cbDairy))).setText("Dairy");
        ((CheckBox) (findViewById(R.id.cbEgg))).setText("Eggs");
        ((CheckBox) (findViewById(R.id.cbGluten))).setText("Gluten");
        ((CheckBox) (findViewById(R.id.cbGrain))).setText("Grains");
        ((CheckBox) (findViewById(R.id.cbPeanut))).setText("Peanuts");
        ((CheckBox) (findViewById(R.id.cbSeafood))).setText("Seafood");
        ((CheckBox) (findViewById(R.id.cbSesame))).setText("Sesame");
        ((CheckBox) (findViewById(R.id.cbShellfish))).setText("Shellfish");
        ((CheckBox) (findViewById(R.id.cbSoy))).setText("Soy");
        ((CheckBox) (findViewById(R.id.cbSulfite))).setText("Sulfites");
        ((CheckBox) (findViewById(R.id.cbTreeNut))).setText("Tree nuts");
        ((CheckBox) (findViewById(R.id.cbWheat))).setText("Wheat");

        ((Button) (findViewById(R.id.saveButton))).setText("Save allergies");
        ((findViewById(R.id.saveButton))).setOnClickListener(MyPreferences.this);

        /** TODO: Add support for getting recipes with Allergies in mind
         * sadly the recipes/random call can not do this, which means that a specific recipe
         * call has to be done, and then repeated X times depending on the day.
         *
         * This could be solved by implementing another method to use when allergies have been added
         *
         * Functionality for saving allergies, and to use them in order to find fitting recipes are present in the project.
         * */
        boolean isAllergyHandlingImplemented = false;
        if (isAllergyHandlingImplemented) {
            // perform necessary changes to the code in order to support allergenic generation
        } else {
            findViewById(R.id.rightAllergyLayout).setEnabled(false);
            findViewById(R.id.rightAllergyLayout).setEnabled(false);

            ((CheckBox) (findViewById(R.id.cbDairy))).setEnabled(false);
            ((CheckBox) (findViewById(R.id.cbEgg))).setEnabled(false);
            ((CheckBox) (findViewById(R.id.cbGluten))).setEnabled(false);
            ((CheckBox) (findViewById(R.id.cbGrain))).setEnabled(false);
            ((CheckBox) (findViewById(R.id.cbPeanut))).setEnabled(false);
            ((CheckBox) (findViewById(R.id.cbSeafood))).setEnabled(false);
            ((CheckBox) (findViewById(R.id.cbSesame))).setEnabled(false);
            ((CheckBox) (findViewById(R.id.cbShellfish))).setEnabled(false);
            ((CheckBox) (findViewById(R.id.cbSoy))).setEnabled(false);
            ((CheckBox) (findViewById(R.id.cbSulfite))).setEnabled(false);
            ((CheckBox) (findViewById(R.id.cbTreeNut))).setEnabled(false);
            ((CheckBox) (findViewById(R.id.cbWheat))).setEnabled(false);

            ((Button) (findViewById(R.id.saveButton))).setEnabled(false);
        }

        // add diet-options to spinner
        final Spinner spinner = (Spinner) findViewById(R.id.dietSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                saveDiet(spinner.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        // restore prefs
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, 0);
        final String sharedDiet = prefs.getString("diet", null);

        if (sharedDiet != null) {
            for (int i = 0; i < spinnerArray.length; i++) {
                if (sharedDiet.equals(spinnerArray[i])) {
                    spinner.setSelection(i);
                    break;
                }
            }
        }

        String sharedAllergies = prefs.getString("allergies", null); // get Stringified JSONArray containing preferences

        if (sharedAllergies != null) {
            try {
                JSONObject allergies = new JSONObject(sharedAllergies);

                LinearLayout leftLL = findViewById(R.id.leftAllergyLayout);
                int leftCount = leftLL.getChildCount();

                for (int i = 0; i < leftCount; i++) {
                    CheckBox cb = (CheckBox) leftLL.getChildAt(i);
                    cb.setChecked(allergies.getBoolean(cb.getText().toString()));
                }

                LinearLayout rightLL = findViewById(R.id.rightAllergyLayout);
                int rightCount = rightLL.getChildCount();

                for (int i = 0; i < rightCount; i++) {
                    CheckBox cb = (CheckBox) rightLL.getChildAt(i);
                    cb.setChecked(allergies.getBoolean(cb.getText().toString()));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public void onClick(View view) {
        Log.d("PREFS", "JUST CLICKED " + view.getId());
        if (view.getId() == R.id.saveButton) {
            saveAllergies();
        }
    }
    void saveDiet(String dietToSave) {
        Log.i("diettosave", dietToSave);
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("diet", dietToSave);
        editor.apply();
    }
    void saveAllergies() {
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = prefs.edit();

        LinearLayout leftLL = findViewById(R.id.leftAllergyLayout);
        int leftCount = leftLL.getChildCount();
        LinearLayout rightLL = findViewById(R.id.rightAllergyLayout);
        int rightCount = rightLL.getChildCount();

        JSONObject allergies = new JSONObject();

        for (int i = 0; i < leftCount; i++) {
            CheckBox cb = (CheckBox) leftLL.getChildAt(i);
            try {
                allergies.put(cb.getText().toString(), cb.isChecked());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < rightCount; i++) {
            CheckBox cb = (CheckBox) rightLL.getChildAt(i);
            try {
                allergies.put(cb.getText().toString(), cb.isChecked());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Log.i("PREFS", allergies.toString());
        editor.putString("allergies", allergies.toString()); // add JSONArray with Diet here
        editor.apply();
    }
}
