package se.ju.foodlist;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import static se.ju.foodlist.Recipes.recipes;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener {

    JSONObject recipeForToday;
    JSONObject surpriseObject;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //region Auto-generated stuffings
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //endregion
        Log.i("#############################", "onCreate!");
        SearchView SV = (SearchView) findViewById(R.id.searchView);
        ImageView IV = (ImageView) findViewById(R.id.todaysFoodImage);
        Button btn = (Button) findViewById(R.id.surpriseButton);

        btn.setOnClickListener(MainActivity.this);
        IV.setClickable(true);
        IV.setOnClickListener(MainActivity.this);

        SV.setOnQueryTextListener(MainActivity.this);

        // todo: setup search, segue to search-results with the given search-string

    }
    /*@Override
    protected void onDestroy() {
        Log.i("SUICIDE", "About to kill meself. Tell my wife I love her!");
        try {
            Storage.Save(Recipes.recipes, utils.getTodaysWeek(), this.getApplicationContext(), utils.getToastCallback(this.getApplicationContext()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }*/
    @Override
    protected void onResume() {
        super.onResume();

        if (recipes != null) {
            recipeForToday = recipes[utils.getTodaysDay()];

            if (recipeForToday != null) {
                Log.i("#############################", "onResume!");
                showTodaysRecipe(recipeForToday);
            }
        }
    }
    public void onClick(View view) {
        if (view.getId() == R.id.todaysFoodImage) {
            Intent intent = new Intent(this, RecipeDetail.class);
            intent.putExtra("recipe", recipeForToday.toString());
            startActivity(intent);
        } else if (view.getId() == R.id.surpriseButton) {
            Intent intent = new Intent(this, RecipeDetail.class);

            AsyncTask<String, Integer, JSONObject> recipeObject = new API.Get().execute("recipes/random");
            try {
                JSONObject jsonObject = recipeObject.get();
                surpriseObject = (JSONObject)jsonObject.getJSONArray("recipes").get(0);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            intent.putExtra("recipe", surpriseObject.toString());
            startActivity(intent);
        }
    }
    @Override
    public boolean onQueryTextSubmit(String s) {
        // todo: send this shit to search-result and get results there
        Log.i("#############################", "TextSubmit: " + s);
        Intent intent = new Intent(this, SearchResult.class);
        intent.putExtra("searchQuery", s);
        startActivity(intent);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        Log.i("#############################", "Text changed...");
        return false;
    }

    // TEMPLATE STUFF
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        //todo: change ID:to correctly match activities
        if (id == R.id.nav_camera) {
            // start MyWeekOfFood-Activity
            Intent intent = new Intent(this, MyWeekOfFood.class);
            startActivity(intent);
        } else if (id == R.id.nav_gallery) {
            // start MyPreferences-Activity
            Intent intent = new Intent(this, MyPreferences.class);
            startActivity(intent);
        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_grocery) {
            // start GroceryList-Activity
            Intent intent = new Intent(this, GroceryList.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    void showTodaysRecipe (JSONObject recipe) {
        Log.i("#############################", "showTodaysRecipe!");
        TextView TV = (TextView) findViewById(R.id.todaysMealLabel);
        final ImageView IV = (ImageView) findViewById(R.id.todaysFoodImage);


        try {
            TV.setText(recipe.get("title").toString());
            Handler handler = new Handler(new Handler.Callback() {
                @Override
                public boolean handleMessage(Message message) {
                    IV.setImageBitmap((Bitmap)message.obj);
                    return true;
                }
            });
            API.getBitmapFromURL(recipe.getString("image"), handler);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
