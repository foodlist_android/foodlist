package se.ju.foodlist;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

public class GroceryList extends AppCompatActivity implements View.OnClickListener {
    JSONObject[] arrayOfRecipesForGroceryList = new JSONObject[7];
    JSONObject recipeObject;
    static final int PERMISSION_OK = 666;
    GPSTracker gps = new GPSTracker(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grocery_list);

        Button btn = findViewById(R.id.findStoreButton);
        btn.setOnClickListener(GroceryList.this);

        if (Recipes.recipes == null) {
            // ToDo: Show text that you need to generate a week before creating a grocery list
            Log.e("GroceryList", "NOT IMPLEMENTED");
        } else {
            if (Recipes.grocertList == null) {
                try {
                    Recipes.CreateGroceryListFromRecipes();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            fillGroceryList(Recipes.grocertList);
        }

        /*
        development
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            Log.i("##############################","extras != true");
            for (int i = 0; i < 7; i++) {
                String value = extras.getString("recipe" + i);
                try {
                    recipeObject = new JSONObject(value);
                    arrayOfRecipesForGroceryList[i] = recipeObject;
                    Log.i("##############################", "" + arrayOfRecipesForGroceryList[i]);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            fillGroceryList(addGroceries(arrayOfRecipesForGroceryList));
        }
        */
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.findStoreButton) {
            // todo: show GPS with users location, and location to food-stores
            gps.getLocation();
            if (gps.canGetLocation()) {
                startGoogleMaps();
            } else {
                gps.showSettingsAlert();
                if (gps.canGetLocation()) {
                    startGoogleMaps();
                }
            }
        }
    }

    public void startGoogleMaps() {
        double latitude = gps.getLatitude();
        double longitude = gps.getLongitude();
        Log.i("URI", "geo:"+ latitude + "," + longitude + "?q=" + Uri.encode("supermarket"));

        Uri gmmIntentUri = Uri.parse("geo:"+ latitude + "," + longitude + "?q=" + Uri.encode("supermarket"));
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");

        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }

    public void findStoresNearUser() {
        Log.i("XXXXXXXXXXXX", "" + gps.getLongitude() + "||||" + gps.getLatitude());

    }

    ArrayList<JSONObject> addGroceries(JSONObject[] arrayOfRecipesForGroceryList) {
        Log.i("##############################","Going in to addGroceries");
        ArrayList<JSONObject> listOfIngredients = new ArrayList<JSONObject>();
        try {
            for (int i = 0; i < 7; i++) {
                JSONObject recipe = arrayOfRecipesForGroceryList[i];
                if (recipe.has("extendedIngredients")) { // if there's a recipe on the specified element
                    JSONArray ingredients = (JSONArray) arrayOfRecipesForGroceryList[i].get("extendedIngredients");

                    for (int n = 0; n < ingredients.length(); n++) {
                        Boolean foundIngredient = false;
                        int x;

                        for (x = 0; x < listOfIngredients.size(); x++) {
                            if (((JSONObject)ingredients.get(n)).getInt("id") == listOfIngredients.get(x).getInt("id")) {
                                foundIngredient = true;
                                break;
                            }
                        }

                        if (foundIngredient) {
                            // Todo: Create has checks
                            int amount = listOfIngredients.get(x).getInt("amount");
                            listOfIngredients.get(x).put("amount", amount + ((JSONObject)ingredients.get(n)).getInt("amount"));
                        }
                        else {
                            // add ingredient
                            listOfIngredients.add((JSONObject) ingredients.get(n));
                            Log.i("##############################", "ingredients.get(n)" + ((JSONObject) ingredients.get(n)).get("name").toString());
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return listOfIngredients;
    }

    void fillGroceryList(ArrayList<JSONObject> ingredients) {
        Log.i("##############################","Going in to fillGroceryList");
        TableLayout TL = (TableLayout) findViewById(R.id.groceryTableLayout);
        int childCount = TL.getChildCount();
        if (childCount > 1) {TL.removeViews(0, childCount);}

        for (int i = 0; i < ingredients.size(); i++) {
            // todo: fix the focken errors mate
            TableRow row = new TableRow(this);
            TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
            row.setLayoutParams(lp);

            TextView nameLabel = new TextView(this);
            TextView amountLabel = new TextView(this);
            TextView unitLabel = new TextView(this);
            try {
                nameLabel.setText(ingredients.get(i).get("name").toString());
                float amount = ingredients.get(i).getInt("amount");
                amountLabel.setText("   " + String.format("%.2f", amount) + "   ");
                unitLabel.setText(ingredients.get(i).get("unitLong").toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            nameLabel.setTextSize(15);
            amountLabel.setTextSize(15);
            unitLabel.setTextSize(15);


            row.addView(nameLabel);
            row.addView(amountLabel);
            row.addView(unitLabel);
            TL.addView(row, i);
        }
    }
}
