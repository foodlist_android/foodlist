package se.ju.foodlist;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by denni on 2017-12-08.
 */

public abstract class Recipes {
    public static JSONObject[] recipes = null;

    public static ArrayList<JSONObject> grocertList = null;

    public static void CreateGroceryListFromRecipes() throws JSONException {
        Log.d("##############################", "I am now going to create gcl from recipes...");
        if (recipes == null) {
            throw new JSONException("Recipes is null");
        }

        ArrayList<JSONObject> ingredients = new ArrayList<JSONObject>();
        for (int i = 0; i < recipes.length; i++) {
            if (recipes[i].has("extendedIngredients")) {
                JSONArray currentRecipeIngredients = recipes[i].getJSONArray("extendedIngredients");
                int size = currentRecipeIngredients.length();

                for (int j = 0; j < size; j++) {
                    ingredients.add((JSONObject)currentRecipeIngredients.get(j));
                }
            }
        }
        Log.i("###########################", "recipes is not null!" + ingredients.size());
        grocertList = ingredients;
    }
}
