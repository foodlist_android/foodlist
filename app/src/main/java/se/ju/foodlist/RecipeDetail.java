package se.ju.foodlist;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

public class RecipeDetail extends AppCompatActivity {
    JSONObject recipeObject;
    JSONArray ingredientRecipeObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_detail);

        TextView recipeName = findViewById(R.id.recipeNameLabel);
        TextView recipeIngredients = findViewById(R.id.recipeIngredientsLabel);
        TextView recipeInstructions = findViewById(R.id.recipeRecipeLabel);
        final ImageView recipeImage = findViewById(R.id.recipeFoodImage);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String value = extras.getString("recipe");
            Log.i("sent via intent", "" + value);
            try {
                recipeObject =  new JSONObject(value);
                ingredientRecipeObject = recipeObject.getJSONArray("extendedIngredients");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        try {
            Handler handler = new Handler(new Handler.Callback() {
                @Override
                public boolean handleMessage(Message message) {
                    recipeImage.setImageBitmap((Bitmap)message.obj);
                    return true;
                }
            });
            String ingredientString = "";
            int amountOfRecipes = ingredientRecipeObject.length();
            for (int i = 0; i < amountOfRecipes; i++) {
                String ingredient = (i + 1) + ". " + (((JSONObject) ingredientRecipeObject.get(i)).get("originalString"));
                ingredientString +=  "\n" + ingredient;
            }
            recipeName.setText(recipeObject.get("title").toString());
            API.getBitmapFromURL(recipeObject.get("image").toString(), handler);
            recipeIngredients.setText(ingredientString);
            recipeInstructions.setText(recipeObject.get("instructions").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
