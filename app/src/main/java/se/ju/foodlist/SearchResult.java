package se.ju.foodlist;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class SearchResult extends AppCompatActivity implements View.OnClickListener{

    String query;
    int amountOfResults = 15;
    int amountOfRecipesGotten = 0;
    JSONObject[] arrayOfSearchResults = new JSONObject[amountOfResults];
    String imageBaseUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        for (int i = 0; i < amountOfResults; i++) {arrayOfSearchResults[i] = new JSONObject();} // fill the array with placeholders

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            query = extras.getString("searchQuery");
            Log.i("sent via intent", "" + query);
        }
        // '/recipes/search?number=10&query=burger&type=main+course'

        String checkedString = "";
        for (int i = 0; i < query.length(); i++) { checkedString += (query.charAt(i) == ' ') ? '+' : query.charAt(i); }
        if (checkedString.length() > 1) {
            getSearchResults(checkedString);
            populateSearchTable();
        } else {
            Log.e("XXX-|ERROR|-XXX", "Query is not valid!");
        }
    }

    @Override
    public void onClick(View view) {
        JSONObject fullRecipe = new JSONObject();
        Intent intent;
        Log.i("#### onClick || searchResult ####", "" + view.getId());
        try {
            int idForClickedRecipe = (int) arrayOfSearchResults[view.getId()].get("id");
            AsyncTask<String, Integer, JSONObject> recipe = new API.Get().execute("recipes/" + idForClickedRecipe + "/information");
            fullRecipe = recipe.get();

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        if (fullRecipe.has("title")) {
            intent = new Intent(this, RecipeDetail.class);
            intent.putExtra("recipe", fullRecipe.toString());
            Log.i("recipe.toString()", "" + fullRecipe.toString());
            startActivity(intent);
        } else Log.e("#### ERROR ####", "JSON is devoid of 'title'");
    }


    // todo: function that takes the search-query and gets X amount of search-results
    void getSearchResults(String query) {
        AsyncTask<String, Integer, JSONObject> recipeObject = new API.Get().execute("recipes/search?number=" + amountOfResults + "&query=" + query + "&type=main+course");
        try {
            JSONObject jsonObject = recipeObject.get();
            amountOfRecipesGotten = jsonObject.getJSONArray("results").length();
            Log.i("amounr of recipes gotten", "" + amountOfRecipesGotten);

            for (int i = 0; i < amountOfRecipesGotten; i++) {
                imageBaseUri = jsonObject.get("baseUri").toString();
                arrayOfSearchResults[i] = (JSONObject) jsonObject.getJSONArray("results").get(i);
                Log.i("XXX-|Results of search|-XXX", "" + arrayOfSearchResults[i].get("title"));
            }

            // todo: function that takes X amount of search-results and populates a table with them
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    void populateSearchTable() {
        TableLayout TL = findViewById(R.id.resultsTable);

        int childCount = TL.getChildCount();
        if (childCount > 1) {TL.removeViews(0, childCount);}

        for (int i = 0; i < amountOfRecipesGotten; i++) {

            TableRow row = new TableRow(this);
            TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
            row.setLayoutParams(lp);
            row.setId(i);
            row.setClickable(true);
            row.setOnClickListener(SearchResult.this);

            TextView TV = new TextView(this);
            final ImageView IV = new ImageView(this);

            try {
                TV.setText(arrayOfSearchResults[i].get("title").toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                Handler handler = new Handler(new Handler.Callback() {
                    @Override
                    public boolean handleMessage(Message message) {
                        Log.i("Callback", "The bitmat! " + ((Bitmap)message.obj).getByteCount());
                        IV.setImageBitmap(Bitmap.createScaledBitmap((Bitmap)message.obj, 120, 120, false));
                        return true;
                    }
                });
                API.getBitmapFromURL(imageBaseUri + arrayOfSearchResults[i].getJSONArray("imageUrls").get(0).toString(), handler);
            } catch (Exception e) {
                e.printStackTrace();
            }

            TV.setTextSize(11); // this is wrong, should be set to a percentage instead
            // todo: fix image-size of IV

            row.addView(IV);
            row.addView(TV);
            TL.addView(row, i);
        }
    }
}
