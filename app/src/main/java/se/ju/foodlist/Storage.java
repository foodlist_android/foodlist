package se.ju.foodlist;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by denni on 2017-12-07.
 */

public abstract class Storage {
    private static final String FILENAME = "recipes.txt";

    // Todo: Removed this if not used
    private static final String getFileName(Context context) {
        return context.getFilesDir().getPath().toString() + "/" + FILENAME;
    }

    private static Boolean bSaving = false;

    public static void Save(final JSONObject[] recipes, final int week, final Context context, final Handler handler) throws IOException, JSONException {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.i("Storage", "Is in save!");
                try {
                    JSONObject allRecipes = Storage.Get(context);

                    if (!allRecipes.has(String.valueOf(week))) {
                        allRecipes.put(String.valueOf(week), new JSONObject());
                    }

                    for (int i = 0; i < recipes.length; i++) {
                        allRecipes.getJSONObject(String.valueOf(week)).put(String.valueOf(i), recipes[i]);
                    }

                    Save(allRecipes, context);

                    Message message = new Message();
                    message.obj = "Successfully saved the week!";
                    handler.sendMessage(message);
                } catch (Exception e) {
                    Message message = new Message();
                    message.obj = "There was an error saving this week...";
                    handler.sendMessage(message);
                }
            }
        }).start();
    }

    public static void Save(JSONObject recipe, int week, int day, Context context) throws IOException, JSONException {
        JSONObject recipes = Get(context);

        if (recipes.has(String.valueOf(week))) {
            recipes.getJSONObject(String.valueOf(week)).put(String.valueOf(day), recipe);
        } else {
            JSONObject recipesForWeek = new JSONObject();
            recipesForWeek.put(String.valueOf(day), recipe);

            recipes.put(String.valueOf(week), recipesForWeek);
        }

        Save(recipes, context);
    }

    private static void Save(JSONObject allRecipes, Context context) throws IOException {
        Log.i("Storage", "Going to save this: " + allRecipes.toString());
        //new FileOutputStream(getFileName(context), false).close(); // Create the file if it does not exist


        FileOutputStream stream = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
        stream.write(allRecipes.toString().getBytes());
        stream.close();
    }

    public static JSONObject[] Get(int week, Context context) throws IOException, JSONException {
        JSONObject recipesForWeek = Get(context).getJSONObject(String.valueOf(week));
        JSONObject[] recipes = new JSONObject[7];

        for (int i = 0; i < 7; i++) recipes[i] = recipesForWeek.has(String.valueOf(i)) ? (JSONObject)recipesForWeek.get(String.valueOf(i)) : new JSONObject();

        return recipes;
    }

    public static JSONObject Get(int week, int day, Context context) throws IOException, JSONException {
        return Get(context).getJSONObject(String.valueOf(week)).getJSONObject(String.valueOf(day));
    }

    private static JSONObject Get(Context context) throws IOException, JSONException {
        new FileOutputStream(getFileName(context), true).close(); // Create the file if it does not exist
        FileInputStream stream = context.openFileInput(FILENAME);

        int character;
        String line = "";

        while ((character = stream.read()) != -1) {
            line += (char)character;
        }

        Log.i("Storage", "Just read the line = " + line);
        stream.close();
        return new JSONObject(line == "" ? "{}" : line);
    }
}
