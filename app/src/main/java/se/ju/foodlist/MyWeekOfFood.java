package se.ju.foodlist;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.content.Intent;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class MyWeekOfFood extends AppCompatActivity implements View.OnClickListener{
    public static final String PREFS_NAME = "MyPrefsFile";
    SharedPreferences prefs;
    String diet = "";
    JSONObject[] arrayOfRecipes = new JSONObject[7];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_week_of_food);
        prefs = getSharedPreferences(PREFS_NAME, 0);

        TableLayout TL = findViewById(R.id.myWeekTable); // The table containing the days
        Button newWeekButton = findViewById(R.id.newWeekButton);
        Button addGroceryButton = findViewById(R.id.addGroceryButton);
        TextView weekLabel = findViewById(R.id.weekLabel);

       weekLabel.setText("Week " + String.valueOf(utils.getTodaysWeek()));

        // add click-listeners
        newWeekButton.setOnClickListener(MyWeekOfFood.this);
        addGroceryButton.setOnClickListener(MyWeekOfFood.this);

        if (Recipes.recipes != null) {
            // there exists something in storage
            arrayOfRecipes = Recipes.recipes;
            populateTable(TL);  // add rows dynamically to the tablelayout
        } else {
            arrayOfRecipes = generateNewWeek();
            populateTable(TL);  // add rows dynamically to the tablelayout
            // Got recipes from web, try to save these
            saveWeek();
        }
    }
    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.addGroceryButton: // Add to groceries
                intent = new Intent(this, GroceryList.class);
                for (int i = 0; i < arrayOfRecipes.length; i++) { intent.putExtra("recipe" + i, arrayOfRecipes[i].toString());}
                startActivity(intent);
                break;
            case R.id.newWeekButton: // Generate a new week
                arrayOfRecipes = generateNewWeek();
                saveWeek();
                TableLayout TL = findViewById(R.id.myWeekTable);
                populateTable(TL);
                break;
            default:
                JSONObject recipe = arrayOfRecipes[view.getId()];
                if (recipe.has("title")) {
                    intent = new Intent(this, RecipeDetail.class);
                    intent.putExtra("recipe", recipe.toString());
                    Log.i("recipe.toString()", "" + recipe.toString());
                    startActivity(intent);
                } else Log.e("#### ERROR ####", "JSON is devoid of 'title'");
                break;
        }
    }
    void populateTable(TableLayout TL) {
        int today = utils.getTodaysDay();

        int childCount = TL.getChildCount();
        if (childCount > 0) { TL.removeViews(0, childCount); }

        for (int i = today; i < 7; i++) {
            TableRow row = new TableRow(this);
            TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT);
            row.setLayoutParams(lp);
            row.setId(i);
            row.setClickable(true);
            row.setOnClickListener(MyWeekOfFood.this);

            final ImageView IV = new ImageView(this);
            TextView TV = new TextView(this);

            // Todo: Is image correct for getting URL string?
            try {
                Handler handler = new Handler(new Handler.Callback() {
                @Override
                public boolean handleMessage(Message message) {
                IV.setImageBitmap((Bitmap)message.obj);
                return true;
                }
                });
                API.getBitmapFromURL(((JSONObject)arrayOfRecipes[i]).get("image").toString(), handler);

                // IV.setImageBitmap(getBitmapFromURL(((JSONObject)arrayOfRecipes[0].getJSONArray("recipes").get(0)).get("image").toString()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Log.i("##### I value #####", "" + i + ": "+ arrayOfRecipes[i].get("title").toString());
                TV.setText((arrayOfRecipes[i].get("title").toString()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            TV.setTextSize(15);

            row.addView(IV);
            row.addView(TV);
            TL.addView(row, i - today);
        }
    }
    private void saveWeek() {
        final MyWeekOfFood self = this;
        try {
            final Button btn = findViewById(R.id.newWeekButton);
            btn.setEnabled(false);
            Storage.Save(arrayOfRecipes, utils.getTodaysWeek(), this.getApplicationContext(), new Handler(Looper.getMainLooper()) {
                @Override
                public void handleMessage(Message message) {
                    Log.i("Response", "Just got response, the message is: " + (String)message.obj);
                    Toast toast = Toast.makeText(self.getApplicationContext(), (String)message.obj, Toast.LENGTH_LONG);
                    toast.show();
                    btn.setEnabled(true);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private JSONObject[] generateNewWeek() {
        JSONObject[] arrayOfRecipes = new JSONObject[7];

        final String sharedDiet = prefs.getString("diet", null);

        // todo: diet
        if (sharedDiet != null && !(sharedDiet.equals("All-eater,"))) {
            // diet is in place, check which
            if (sharedDiet.equals("Vegetarian")) diet = "vegetarian,";
            if (sharedDiet.equals("Vegan")) diet = "vegan,";
            if (sharedDiet.equals("Primal")) diet = "primal,";
            if (sharedDiet.equals("Ketogenic")) diet = "ketogenic,";
            if (sharedDiet.equals("Paleo")) diet = "paleo,";
        }

        // if no recipes in storage, fill up the empty days
        int today = utils.getTodaysDay();
        Log.i("today:", "" + today);

        for (int i = 0; i < today; i++) {arrayOfRecipes[i] = new JSONObject();}

        AsyncTask<String, Integer, JSONObject> recipeObject = new API.Get().execute("recipes/random?number=" + (7 - today) + "&tags=" + diet + "dinner");

        try {
            JSONObject jsonObject = recipeObject.get();
            for (int i = today; i < 7; i++) {
                Log.i("recipe name: ", ((JSONObject) (jsonObject.getJSONArray("recipes").get(i - today))).get("title").toString());
                arrayOfRecipes[i] = (JSONObject) jsonObject.getJSONArray("recipes").get(i - today);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        // Todo: When generating a new week, the grocery list is out of date. Give a message to the user that it is outdated. Might be implemented by using a flag in Recipes
        Recipes.recipes = arrayOfRecipes;
        return arrayOfRecipes;
    }
}
