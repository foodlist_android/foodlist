package se.ju.foodlist;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONObject;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Log.i("Splash", "This is on create!");

        final Context context = this.getApplicationContext();
        // Load recipes from storage. While loading, show splashscreen
        Thread welcomeThread = new Thread() {
            @Override
            public void run() {
                JSONObject[] recipes = null;
                try {
                    super.run();
                    Recipes.recipes = Storage.Get(utils.getTodaysWeek(), context);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        };
        welcomeThread.start();
    }
    @Override
    protected void onDestroy() {
        /*
        Log.i("Destroy", "About to kill myself... Tell my wife I love her!");
        try {
            Storage.Save(Recipes.recipes, utils.getTodaysWeek(), this.getApplicationContext(), utils.getToastCallback(this.getApplicationContext()));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
        super.onDestroy();
    }
}
